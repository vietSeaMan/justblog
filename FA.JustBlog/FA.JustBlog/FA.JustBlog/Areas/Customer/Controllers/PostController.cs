﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Models.ViewModels;
using FA.JustBlog.Core.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using FA.JustBlog.Core.Utility;
using Microsoft.AspNetCore.Authorization;


namespace FA.JustBlog.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize]
    public class PostController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        IEnumerable<Post> postList;
        public PostController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index(IList<PostVM>? listPostVm = null)
        {
           
            //Check if ListPostVM have no element the show all Post List
            if (listPostVm.Count <= 0)
            {
                IList<PostVM> postVMs = new List<PostVM>();
                postList = _unitOfWork.Post.GetAll();
                foreach (var post in postList)
                {
                    postVMs.Add(new PostVM()
                    {
                        Post = post,
                        TimeSpan = Helper.TimeCompare(post.PostedOn),
                        
                    });

                }
                ViewBag.Message = "";
                return View(postVMs);
            }
            //else show listPost by condition => See more at GetPostByCategoryId, LastestPost,.. Action
            return View(listPostVm);
        }


        [HttpPost]
        public IActionResult Detail(int id)
        {
            var post = _unitOfWork.Post.GetFirstOrDefault(p => p.Id == id);

            var postVm = new PostVM()
            {
                Post = post,
                TagList = _unitOfWork.Tag.GetTagByPost(post.Id).ToList(),
                Category = _unitOfWork.Category.GetFirstOrDefault(c => c.Id == post.CategoryId)
            };

            return View(postVm);
        }

        //[Route("Post/{year}/{month}/{title}")]
        
        public IActionResult Detail(int year, int month, string urlSlug)
        {
            var post = _unitOfWork.Post.Find(year, month, urlSlug);

            //Incre View
            post.ViewCount++;
            _unitOfWork.Post.Update(post);
            _unitOfWork.Save();

            if (post == null)
                return NotFound();

            var postVm = new PostVM()
            {
                Post = post,
                TagList = _unitOfWork.Tag.GetTagByPost(post.Id).ToList(),
                Category = _unitOfWork.Category.GetFirstOrDefault(c => c.Id == post.CategoryId)

            };
            return View(postVm);
        }

        public IActionResult LastestPost()
        {
            IList<PostVM> postVMs = new List<PostVM>();
            postList = _unitOfWork.Post.GetAll().OrderByDescending(p => p.PostedOn);
            foreach (var post in postList)
            {
                postVMs.Add(new PostVM()
                {
                    Post = post,
                    TimeSpan = Helper.TimeCompare(post.PostedOn),
                });
            }
            ViewBag.Message = "Lastest - ";
            return View("Index", postVMs);
        }


        public IActionResult GetPostByCategoryId(int id)
        {
            IList<PostVM> postVMs = new List<PostVM>();
            postList = _unitOfWork.Post.GetAll(p => p.CategoryId == id);
            foreach (var post in postList)
            {
                postVMs.Add(new PostVM()
                {
                    Post = post,
                    TimeSpan = Helper.TimeCompare(post.PostedOn)
                  
                });
            }
            ViewBag.Message = _unitOfWork.Category.GetFirstOrDefault(c => c.Id == id).Name + " - ";
            return View("Index", postVMs);
        }

        public IActionResult GetPostByTagId(int id)
        {
            IList<PostVM> postVMs = new List<PostVM>();
            var tag = _unitOfWork.Tag.GetFirstOrDefault(c => c.Id == id);
            postList = _unitOfWork.Post.GetPostsByTag(tag.Name);
            foreach (var post in postList)
            {
                postVMs.Add(new PostVM()
                {
                    Post = post,
                    TimeSpan = Helper.TimeCompare(post.PostedOn)
                });
            }
            ViewBag.Message = tag.Name + " - ";
            return View("Index", postVMs);
        }

        
    }
}
