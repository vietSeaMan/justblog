﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Models.ViewModels;
using FA.JustBlog.Core.Repository;
using FA.JustBlog.Core.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using X.PagedList;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Contributor")]
    public class PostController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        IEnumerable<Post> postList;
        public PostController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        
        public IActionResult Index(int? page = null, int? pageSize = null)
        {
            if (page == null)
                page = 1;

            if (pageSize == null)
                pageSize = 5;

            IList<PostVM> postVMs = new List<PostVM>();

            postList = _unitOfWork.Post.GetAll();
            foreach (var post in postList)
            {
                postVMs.Add(new PostVM()
                {
                    Post = post,
                    TimeSpan = Helper.TimeCompare(post.PostedOn)
                });
            }

            return View(postVMs.ToPagedList((int)page, (int)pageSize));
        }

        //Create
        //Get
       
        public IActionResult Create()
        {
            PostVM postVM = new()
            {
                Post = new(),
                CategoryList = _unitOfWork.Category.GetAll().Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                }),
                TagList = _unitOfWork.Tag.GetAll().ToList()

            };
            
            return View(postVM);
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Create(PostVM obj)
        {
            if (ModelState.IsValid)
            {
                //Set Published
                obj.Post.Published = false;
                if (obj.Published == "true")
                    obj.Post.Published = true;
                
                //Set UrlSlug
                var stringArr = obj.Post.Title.Split(' ');
                var urlSlug = new StringBuilder();
                foreach (var i in stringArr)
                {
                    urlSlug.Append("-"+i);
                }
                
                obj.Post.UrlSlug = SeoUrlHepler.FrientlyUrl(obj.Post.Title);

                _unitOfWork.Post.Add(obj.Post);
                _unitOfWork.Save();


                var lastPost = _unitOfWork.Post.GetAll().OrderByDescending(p => p.Id).FirstOrDefault();
                if(lastPost != null)
                {
                    //add new record to PostTagMap table
                    foreach (var tagName in obj.SelectedTag)
                    {
                        var tag = _unitOfWork.Tag.GetFirstOrDefault(t => t.Name == tagName);
                        if (tag != null)
                        {
                            var postTagMap = new PostTagMap()
                            {
                                PostId = lastPost.Id,
                                TagId = tag.Id
                            };

                            _unitOfWork.PostTagMap.Add(postTagMap);

                        }
                    }
                }
                

                //Add new Post
                
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(obj);
        }

        //Update
        //Get
        public IActionResult Update(int? id)
        {
            if (id == null || id == 0)      
                return NotFound();

            PostVM postVM = new()
            {
                Post = _unitOfWork.Post.GetFirstOrDefault(p => p.Id == id),
                CategoryList = _unitOfWork.Category.GetAll().Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                }),
                TagList = _unitOfWork.Tag.GetAll().ToList()

            };

            return View(postVM);
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(PostVM obj)
        {
            if (ModelState.IsValid)
            {
                //Set Published
                obj.Post.Published = false;
                if (obj.Published == "true")
                    obj.Post.Published = true;

                //Set Modified 
                obj.Post.Modified = DateTime.Now;

                //update PostTagMap table
            
                List<PostTagMap> postTagMapFomrDb = _unitOfWork.PostTagMap.GetAll(t => t.PostId == obj.Post.Id).ToList();
                //Delete Old PostTagMap
                for (var i = 0; i < postTagMapFomrDb.Count(); i++)
                {
                    _unitOfWork.PostTagMap.Delete(postTagMapFomrDb[i]);
                }
                _unitOfWork.Save();

                //Add new Post Tag Map
                foreach (var tagName in obj.SelectedTag)
                {
                    var tag = _unitOfWork.Tag.GetFirstOrDefault(t => t.Name == tagName);
                    if (tag != null)
                    {
                        var postTagMap = new PostTagMap()
                        {
                            PostId = obj.Post.Id,
                            TagId = tag.Id
                        };

                        _unitOfWork.PostTagMap.Add(postTagMap);

                    }
                }

                //Update Post
                _unitOfWork.Post.Update(obj.Post);

                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(obj);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var post = _unitOfWork.Post.GetFirstOrDefault(p => p.Id == id);
            _unitOfWork.Post.Delete(post);

            _unitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
