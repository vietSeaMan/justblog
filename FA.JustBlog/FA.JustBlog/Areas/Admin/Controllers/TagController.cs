﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Models.ViewModels;
using FA.JustBlog.Core.Repository;
using FA.JustBlog.Core.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Contributor")]
    public class TagController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public TagController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        
        public IActionResult Index()
        {
            IEnumerable<Tag> Tags = new List<Tag>();

            Tags = _unitOfWork.Tag.GetAll();

            return View(Tags);
        }

        //Create
        //Get
  
        public IActionResult Create()
        {
            Tag newTag = new Tag();
            
            return View(newTag);
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Create(Tag tag)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.Tag.Add(tag);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(tag);
        }

        //Update
        //Get
        public IActionResult Update(int? id)
        {
            if (id == null || id == 0)      
                return NotFound();

            var tag = _unitOfWork.Tag.GetFirstOrDefault(x => x.Id == id);

            return View(tag);
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Tag tag)
        {
            if (ModelState.IsValid)
            {
                //Update Tag
                _unitOfWork.Tag.Update(tag);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(tag);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var tag = _unitOfWork.Tag.GetFirstOrDefault(p => p.Id == id);
            _unitOfWork.Tag.Delete(tag);

            _unitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
