﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Models.ViewModels;
using FA.JustBlog.Core.Repository;
using FA.JustBlog.Core.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Contributor")]
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        IEnumerable<Category> CategoryList;

        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        
        public IActionResult Index()
        {
            CategoryList = _unitOfWork.Category.GetAll();
            return View(CategoryList);
        }

        //Create
        //Get
       
        public IActionResult Create()
        {
            Category newCategory = new Category();

            return View(newCategory);
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Create(Category category)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.Category.Add(category);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(category);
        }

        //Update
        //Get
        public IActionResult Update(int? id)
        {
            if (id == null || id == 0)
                return NotFound();

            var category = _unitOfWork.Category.GetFirstOrDefault(x => x.Id == id);

            return View(category);
        }

        //Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Category category)
        {
            if (ModelState.IsValid)
            {
                //Update Tag
                _unitOfWork.Category.Update(category);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(category);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var post = _unitOfWork.Post.GetFirstOrDefault(p => p.Id == id);
            _unitOfWork.Post.Delete(post);

            _unitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
