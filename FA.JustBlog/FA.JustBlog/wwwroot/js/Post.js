﻿
var ApiData;
var mainContent;


$(document).ready(function () {
    loadData();
});



function loadData() {
    var mainContent = document.getElementById("PostContent");

    $.ajax({
        type: "GET",
        url: "/Customer/Post/LoadData",
        datatype : "json",
        //async: true,
        success: function (response) {
            
            let list = response.data;
            list.forEach(Post => {
                var postOn = new Date(Post.postedOn);
                var html = `
                <div class="card mb-3"> 
                    <div class="card-body col-12">
                        <a href="/Customer/Post/Detail?year=${postOn.getFullYear()}&month=${postOn.getMonth() + 1}&urlSlug=${Post.urlSlug}">${Post.title}</a>
                        <h6 class="card-subtitle mb-2 text-muted">Rate ${Post.rate} by ${Post.viewCount} view(s)</h6>
                        <p class="card-text">${Post.shortDiscription}</p>
                    </div>
                </div>
                `;
                mainContent.innerHTML += html;
            });
         
        },
        error: function (err) {
            window.alert(err);
        }
    });

    
}