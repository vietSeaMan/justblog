﻿
$(document).ready(function () {
    loadData();
});



function loadData() {
    var mainContent = document.getElementById("CommentsContent");
    var id = $("#PostId").val();
    console.log(id);
    $.ajax({
        type: "GET",
        url: "/Customer/Post/loadComment/"+id,
        datatype : "json",
        //async: true,
        success: function (response) {
            
            let list = response.data;
            list.forEach(Comment => {
                var html = `
                    <h5 class="card-subtitle mb-2 text-muted">By: ${Comment.email}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${Comment.commentHeader}</h6>
                    <p class="card-text">Date: ${Comment.commentTime}</p>
                    <p class="card-text">${Comment.commentText}</p>
                `;
                mainContent.innerHTML += html;
            });
         
        },
        error: function (err) {
            window.alert(err);
        }
    });
}

//$("#addNewComment").click(loadData());