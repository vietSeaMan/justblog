﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Core.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UrlSlug { get; set; }
        public string Discription { get; set; }
        public int Count { get; set; }

        [ValidateNever]
        public virtual IList<PostTagMap> PostTagMaps { get; set; }
    }
}