﻿using FA.JustBlog.Core.Data;
using FA.JustBlog.Core.IRepository;
using FA.JustBlog.Core.Repository;


namespace FA.JustBlog.Core.infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private JustBlogContext context;

        public UnitOfWork(JustBlogContext context)
        {
            this.context = context;
            Category = new CategoryRepository(this.context);
            Post = new PostRepository(this.context);
            Tag = new TagRepository(this.context);
            PostTagMap = new PostTagMapRepository(this.context);
        }

        public ICategoryRepository Category
        {
            get;
            private set;
        }
        public IPostRepository Post
        {
            get;
            private set;
        }

        public ITagRepository Tag
        {
            get;
            private set;
        }
        public IPostTagMapRepository PostTagMap
        {
            get;
            private set;
        }

        public ICommentRepository Comment 
        {
            get;
            private set;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public int Save()
        {
            return context.SaveChanges();
        }
    }
}

