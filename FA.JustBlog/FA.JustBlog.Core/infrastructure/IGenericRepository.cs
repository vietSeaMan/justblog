﻿using FA.JustBlog.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Repository
{
    public interface IGenericRepository<TEntity>
    {
        TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> filler, string? includeProperties = null);

        void Add(TEntity Entity);
        void Delete(TEntity Entity);
        void Delete(int EntityId);
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>>? filler = null, string? includeProperties = null);
    }
}
