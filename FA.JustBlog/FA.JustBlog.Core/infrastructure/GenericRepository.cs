﻿using FA.JustBlog.Core.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace FA.JustBlog.Core.Repository
{

    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        internal JustBlogContext context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(JustBlogContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual void Add(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public void Delete(int entityId)
        {
            TEntity entity = dbSet.Find(entityId);
            Delete(entity);
        }


        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>>? filler = null, string? includeProperties = null)
        {
            IQueryable<TEntity> query = dbSet;
            if (filler != null)
                query = query.Where(filler);

            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }
            return query.ToList();
        }

        public TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> filler, string? includeProperties = null)
        {
            IQueryable<TEntity> query = dbSet;
            query = query.Where(filler);
            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return query.FirstOrDefault()!;
        }
    }
}
