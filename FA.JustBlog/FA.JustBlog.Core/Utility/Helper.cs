﻿namespace FA.JustBlog.Core.Utility
{
    public class Helper
    {
        public static string TimeCompare(DateTime dateTime)
        {
            var timeNow = DateTime.Now;
            var timeDiff = timeNow - dateTime;

            if (timeNow.Day - dateTime.Day == 1 && timeNow.Month == dateTime.Month && dateTime.Year == timeNow.Year)
                return $"yesterday at {dateTime.Hour}:{dateTime.Minute}";

            else if (timeNow.Day - dateTime.Day == 0 && timeNow.Month == dateTime.Month && dateTime.Year == timeNow.Year)
            {
                var h = timeNow.Subtract(dateTime);
                if (h.Hours > 0)
                {
                    return h.Hours.ToString() + " hours ago";
                }
                else
                {
                    return h.Minutes.ToString() + " minutes ago";
                }
            }

            return $"{((int)timeDiff.TotalDays)} days ago";
        }
    }
}
