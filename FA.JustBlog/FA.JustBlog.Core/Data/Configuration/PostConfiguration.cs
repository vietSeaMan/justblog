﻿using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Data.Configuration
{
    public class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Title).IsRequired();
            builder.Property(p => p.PostContent).IsRequired().HasMaxLength(1000);
            builder.Property(p => p.UrlSlug).IsRequired().HasMaxLength(255);
            builder.Property(p => p.Published).IsRequired();

            //part 8 add new fields
            builder.Property(p => p.RateCount).IsRequired();
            builder.Property(p => p.TotalRate).IsRequired();

            //Set one to many relation to Category table and foreignKey
            builder.HasOne(p => p.Category).WithMany(c => c.Posts).HasForeignKey(p => p.CategoryId);

            //Add Seed data
            builder.HasData(
                new Post()
                {
                    Id = 1,
                    Title = "Post 1",
                    ShortDiscription = "Short Discription 1",
                    PostContent = "This is main Content 1",
                    UrlSlug = "Url for Post 1",
                    Published = true,
                    PostedOn = new DateTime(2022, 1, 23),
                    Modified = new DateTime(2022, 3, 1),
                    CategoryId = 1
                },
                new Post()
                {
                    Id = 2,
                    Title = "Post 2",
                    ShortDiscription = "Short Discription 2",
                    PostContent = "This is main Content 2",
                    UrlSlug = "Url for Post 2",
                    Published = true,
                    PostedOn = new DateTime(2022, 2, 23),
                    Modified = new DateTime(2022, 4, 1),
                    CategoryId = 2
                },
                new Post()
                {
                    Id = 3,
                    Title = "Post 3",
                    ShortDiscription = "Short Discription 3",
                    PostContent = "This is main Content 3",
                    UrlSlug = "Url for Post 3",
                    Published = true,
                    CategoryId = 1
                }
            );

        }
    }
}
