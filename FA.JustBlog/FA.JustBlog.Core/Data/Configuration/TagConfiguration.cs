﻿using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Data.Configuration
{
    public class TagConfiguration : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Name).IsRequired().HasMaxLength(30);
            builder.Property(t => t.UrlSlug).IsRequired().HasMaxLength(255);
            builder.HasIndex(t => t.UrlSlug).IsUnique();
            builder.Property(t => t.Discription).IsRequired().HasMaxLength(1000);

            //Add seed data
            builder.HasData(
                new Tag()
                {
                    Id = 1,
                    Name = "Tag 1",
                    UrlSlug = "URL for Tag 1",
                    Discription = "This is Discription for Tag 1",
                    Count = 2
                },
                new Tag()
                {
                    Id = 2,
                    Name = "Tag 2",
                    UrlSlug = "URL for Tag 2",
                    Discription = "This is Discription for Tag 2",
                    Count = 3
                },
                new Tag()
                {
                    Id = 3,
                    Name = "Tag 3",
                    UrlSlug = "URL for Tag 3",
                    Discription = "This is Discription for Tag 3",
                    Count = 1
                }
            );

            
        }
    }
}
