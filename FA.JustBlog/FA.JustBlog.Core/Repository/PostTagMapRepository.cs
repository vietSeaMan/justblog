﻿using FA.JustBlog.Core.Data;
using FA.JustBlog.Core.IRepository;
using FA.JustBlog.Core.Models;
using System.Linq.Expressions;

namespace FA.JustBlog.Core.Repository
{
    public class PostTagMapRepository : GenericRepository<PostTagMap>, IPostTagMapRepository
    {
        private JustBlogContext _db;
        public PostTagMapRepository(JustBlogContext context) : base(context)
        {
            _db = context;  
        }

        public void Update(PostTagMap postTagMap)
        {
            _db.postTagMaps.Update(postTagMap);
        }
    }
}
