﻿using FA.JustBlog.Core.Data;
using FA.JustBlog.Core.IRepository;
using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Core.Repository
{
    public class PostRepository : GenericRepository<Post>, IPostRepository
    {
        private JustBlogContext _db;
        public PostRepository(JustBlogContext context) : base(context)
        {
            _db = context;
        }

        public override void Add(Post entity)
        {
            dbSet.Add(entity);
        }

        public int CountPostsForCategory(string category)
        {
            //return dbSet.Join(context.Categories, p => p.CategoryId, c => c.Id, (p, c) => new { p, c.Name })
            //    .Where(p => p.Name == category).Count();
            return dbSet.Include(x => x.Category).Where(x => x.Category.Name == category).Count();
        }

        public int CountPostsForTag(string tag)
        {
            //return dbSet.Join(context.postTagMaps, p => p.Id, pt => pt.PostId, (p, pt) => new { p, pt.TagId })
            //    .Join(context.Tags, p => p.TagId, t => t.Id, (p, t) => new { p, t.Name })
            //    .Where(p => p.Name == tag).Count();

            return GetPostsByTag(tag).Count();
        }

        public Post Find(int year, int month, string urlSlug)
        {
            return dbSet.Where(p => (p.PostedOn.Year == year) && (p.PostedOn.Month == month) && (p.UrlSlug == urlSlug)).FirstOrDefault();
        }

        public IList<Post> GetLatestPosts(int size)
        {
            return dbSet.OrderByDescending(p => p.PostedOn).Take(size).ToList();
        }


        public IList<Post> GetPostsByCategory(string category)
        {
            return dbSet.Include(x => x.Category).Where(x => x.Category.Name == category).ToList();
        }

        public IList<Post> GetPostsByMonth(DateTime monthYear)
        {
            return dbSet.Where(p => (p.PostedOn.Month == monthYear.Month) && (p.PostedOn.Year == monthYear.Year)).ToList();
        }

        public IList<Post> GetPostsByTag(string tag)
        {
            //return context.Tags.Where(t => t.Name == tag)
            //    .Join(context.postTagMaps, t => t.Id, pt => pt.TagId, (t, pt) => pt)
            //    .Join(dbSet, pt => pt.PostId, p => p.Id, (pt, p) => p).ToList();
;
            return context.Tags.Where(t => t.Name == tag)
                .SelectMany(t => t.PostTagMaps.Select(pt => pt.Post))
                .ToList();

        }

        public IList<Post> GetPublishedPosts()
        {
            return dbSet.Where(p => p.Published).ToList();
        }

        public IList<Post> GetUnPublishedPosts()
        {
            return dbSet.Where(p => !p.Published).ToList();
        }

        public IList<Post> GetHighestPosts(int size)
        {
            return dbSet.OrderByDescending(p => p.Rate).Take(size).ToList();
        }
        
        public IList<Post> GetMostViewedPost(int size)
        {
            return dbSet.OrderByDescending(p => p.ViewCount).Take(size).ToList();
        }

        public void Update(Post post)
        {
            _db.Posts.Update(post);
            //var postFromDb = _db.Posts.FirstOrDefault(p => p.Id == post.Id);
            //if (postFromDb != null)
            //{
            //    postFromDb.Title = post.Title;
            //    postFromDb.ShortDiscription = post.ShortDiscription;
            //    postFromDb.PostContent = post.PostContent;
            //    postFromDb.TotalRate = post.TotalRate;
            //    postFromDb.CategoryId = post.CategoryId;
            //    postFromDb.RateCount = post.RateCount;

            //}
        }
    }
}
