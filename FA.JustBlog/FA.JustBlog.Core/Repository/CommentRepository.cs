﻿using FA.JustBlog.Core.Data;
using FA.JustBlog.Core.IRepository;
using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repository
{
    public class CommentRepository : GenericRepository<Comment>, ICommentRepository
    {
        private JustBlogContext _db;
        public CommentRepository(JustBlogContext context) : base(context)
        {
            _db = context;
        }

        public void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            var newComment = new Comment()
            {
                PostId = postId,
                Name = commentName,
                Email = commentEmail,
                CommentHeader = commentTitle,
                CommentText = commentBody,
                CommentTime = DateTime.Now, 
            };
            throw new NotImplementedException();
        }

        public IList<Comment> GetCommentsForPost(int postId)
        {
            return dbSet.Where(c => c.PostId == postId).ToList();
        }

        public IList<Comment> GetCommentsForPost(Post post)
        {
            return dbSet.Where(c => c.PostId == post.Id).ToList();
        }

        public void Update(Comment comment)
        {
            _db.Comments.Update(comment);
        }
    }
}
