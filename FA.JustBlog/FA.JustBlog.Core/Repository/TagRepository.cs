﻿using FA.JustBlog.Core.Data;
using FA.JustBlog.Core.IRepository;
using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repository
{
    public class TagRepository : GenericRepository<Tag>, ITagRepository
    {
        private JustBlogContext _db;
        public TagRepository(JustBlogContext context) : base(context)
        {
            _db = context;
        }

        public IList<Tag> GetTagByPost(int postId)
        { 
            return _db.postTagMaps.Where(p => p.PostId == postId).
                Join(_db.Tags, p => p.TagId, t => t.Id, (p, t) => t).ToList();
        }

        public Tag getTagByUrlSlug(string urlSlug)
        {
            return dbSet.Where(t => t.UrlSlug == urlSlug).FirstOrDefault();
        }

        public void Update(Tag tag)
        {
            _db.Tags.Update(tag);
        }
    }
}
