﻿using FA.JustBlog.Core.Data;
using FA.JustBlog.Core.IRepository;
using FA.JustBlog.Core.Models;


namespace FA.JustBlog.Core.Repository
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private JustBlogContext _db;
        public CategoryRepository(JustBlogContext context) : base(context)
        {
            _db = context;
        }

        public void Update(Category category)
        {
            _db.Categories.Update(category);
        }
    }
}
