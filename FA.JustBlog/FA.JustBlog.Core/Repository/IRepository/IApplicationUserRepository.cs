﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository;


namespace FA.JustBlog.Core.IRepository
{
    public interface IApplicationUserRepository : IGenericRepository<ApplicationUser>
    {
        void Update(ApplicationUser applicationUser);
    }
}
