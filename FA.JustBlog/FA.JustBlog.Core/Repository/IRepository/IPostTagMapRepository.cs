﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository;
using System.Linq.Expressions;

namespace FA.JustBlog.Core.IRepository
{
    public interface IPostTagMapRepository : IGenericRepository<PostTagMap>
    {
        void Update(PostTagMap postTagMap);
        

    }
}
