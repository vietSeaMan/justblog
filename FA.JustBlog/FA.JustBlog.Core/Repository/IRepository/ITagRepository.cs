﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository;


namespace FA.JustBlog.Core.IRepository
{
    public interface ITagRepository : IGenericRepository<Tag>
    {
        Tag getTagByUrlSlug(string urlSlug);
        IList<Tag> GetTagByPost(int postId);
        void Update(Tag tag);
    }
}
