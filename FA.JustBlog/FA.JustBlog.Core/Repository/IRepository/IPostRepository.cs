﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository;


namespace FA.JustBlog.Core.IRepository
{
    public interface IPostRepository : IGenericRepository<Post>
    {
        Post Find(int year, int month, string urlSlug);

        IList<Post> GetPublishedPosts();
        IList<Post> GetUnPublishedPosts();
        IList<Post> GetLatestPosts(int size);
        IList<Post> GetPostsByMonth(DateTime monthYear);
        IList<Post> GetPostsByCategory(string category);
        IList<Post> GetPostsByTag(string tag);
        int CountPostsForCategory(string category);
        int CountPostsForTag(string tag);

        IList<Post> GetMostViewedPost(int size);
        IList<Post> GetHighestPosts(int size);

        void Update(Post post);

    }
}
